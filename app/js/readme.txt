*** ファイルの構成 ***
main.js
	require.jsによりファイルの読み込み順序を管理するjavascriptファイル
app.js
	アプリケーション全体を管理するjavascriptファイル
config.js
	Google APIを利用するための設定ファイル
gapi.js
	Google APIを利用するための関数群
	Google APIのライブラリではないことに注意する
	Backbone.syncとBackbone.gapiRequestはこのファイルで定義される

*** ファイルの読み込み順序 ***
###########################################
htmlファイル -> main.js -> app.js(gapi.js) -> 各jsファイル
###########################################
ユーザはサーバへアクセスし、htmlファイルを読み込む。
htmlファイルのbodyタグの最後尾にあるscriptタグにより
javascriptファイルの読み込み順序を管理するmain.jsが読み込まれる。
main.jsにより、アプリケーション全体を管理するapp.jsのオブジェクトが作成される。
app.jsにより、Google APIを管理するマネージャが生成され、
同時に読み込まれるgapi.jsにおいてOAuthによる認証処理を経て
ユーザのプライベートデータにアクセスが可能となる。
その後はbackbone.jsによるMV*モデルに従い、必要なjsファイルが読み込まれる。
