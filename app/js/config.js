/** Google APIの設定ファイル
 * javascript originより下層のフォルダでのみGoogle APIが使用可能
 * Google Developers Console [https://console.developers.google.com/project]
 * javascript origin [http://makecolors.info, http://localhost]
 */
define([], function() {
	var config = {};

	config.clientId = '628107758694-39juuv79ue9ch8hueaua09e1oblo1ptu.apps.googleusercontent.com';
	config.apiKey = 'AIzaSyCSX6c0jaQMRpQ5SBTl22EI2zILSsb_0v4';
	// OAuth認証によるGmailのAPIを利用するにあたって全ての権限を持ったスコープ
	config.scopes = ['https://mail.google.com/'].join(',');
	config.apiName = 'gmail'

	return config;
});
