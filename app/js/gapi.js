/** Google API用ファイル
 * 処理の流れ(共通: 認証記録があればこのまま自動ログインできる)
 * ApiManager -> load -> init -> handleClientLoad -> checkAuth -> handleAuthResult
 * 未ログイン時のみ(OAuth認証のボタンクリック後の動作)
 * checkAuth -> handleAuthResult
 *  参考: Google Javascript API "https://developers.google.com/+/web/api/javascript?hl=ja"
 */
define(['config'], function(config) {
  var app;

  /** Google API用のオブジェクトを作成する
   * @param {} _app 今まで利用していたGoogle APIのオブジェクト
   */
  function ApiManager(_app) {
    app = _app;
    this.loadGapi();
  }

  // ApiManagerにBackbone.Eventsを継承(正しくは継承ではなくコピー?)させる
  _.extend(ApiManager.prototype, Backbone.Events);

  /** Google APIのオブジェクトを初期化する
   */
  ApiManager.prototype.init = function() {
    var self = this;

    // Gmail APIにおけるクライアントインターフェースの読み込み
    gapi.client.load(config.apiName, 'v1', function() { /* Loaded */ });

    /** gapiを利用するための準備
     */
    function handleClientLoad() {
      gapi.client.setApiKey(config.apiKey);
      window.setTimeout(checkAuth, 100);
    }

    /** OAuth2.0による承認プロセスの開始
     * 各プロパティの設定と即時モード(アプリケーションの承認確認)への切り替え
     */
    function checkAuth() {
      // 認証と承認を求めるポップアップウィンドウを表示する.承認プロセスが完了したら、handleAuthResultによるコールバック関数を開始する
      gapi.auth.authorize({ client_id: config.clientId, scope: config.scopes, immediate: true }, handleAuthResult);
    }

    /** OAuth2.0による成功、失敗時の行動.
     *  @param {} authResult OAuthの設定オブジェクト
     */
    function handleAuthResult(authResult) {
      var authTimeout;

      // 成功時はreadyのイベントを呼び出しする.
      if (authResult && !authResult.error) {
        
        // Schedule a check when the authentication token expires
        if (authResult.expires_in) {
          authTimeout = (authResult.expires_in - 5 * 60) * 1000;
          setTimeout(checkAuth, authTimeout);
        }

        // 認証ボタンの非表示とサインイン後のコンテンツの表示
        app.views.authView.$el.hide();
        $('#signed-in-container').show();
        self.trigger('ready');
      }

      // 失敗時は認証エラーとOAuth認証するための接続ボタンを表示する. 
      else {
        if (authResult && authResult.error) {
          console.error('Unable to sign in:', authResult.error);
        }

        app.views.authView.$el.show();
      }
    }

    // OAuth認証の接続ボタンクリック時に使用されるポップアップウィンドウによる認証(クッキーに情報が残っていない場合はこちらを通過する)
    this.checkAuth = function() {
      gapi.auth.authorize({ client_id: config.clientId, scope: config.scopes, immediate: false }, handleAuthResult);
    };

    handleClientLoad();
  };

  /** Google API for Javascriptの読み込み
   * 
   */
  ApiManager.prototype.loadGapi = function() {
    var self = this;

    // Don't load gapi if it's already present
    if (typeof gapi !== 'undefined') {
      return this.init();
    }

    require(['https://apis.google.com/js/client.js?onload=define'], function() {
      // Poll until gapi is ready
      function checkGAPI() {
        if (gapi && gapi.client) {
          self.init();
        } else {
          setTimeout(checkGAPI, 100);
        }
      }
      
      checkGAPI();
    });
  };

  /** Google APIによるCRUD非同期処理
   * optionsに関連する関数 [http://backbonejs.org/#API-integration]
   * @param {} method   the CRUD method ("create", "read", "update", or "delete")
   * @param {} model    the model to be saved (or collection to be read)
   * @param {} options  success and error callbacks, and all other jQuery request options
   */
  Backbone.sync = function(method, model, options) {
    var requestContent = {}, request;
    options || (options = {});

    switch (model.url) {
      case 'draft':
        //requestContent.task = model.get('id');
        //requestContent.tasklist = model.get('tasklist');
      break;

      case 'label':
      break;

      case 'thread':
      break;

      case 'message':
      break;
    }

    // CRUD処理
    switch (method) {
      /*
      case 'create':
        requestContent['resource'] = model.toJSON();
        request = gapi.client.tasks[model.url].insert(requestContent);
        Backbone.gapiRequest(request, method, model, options);
      break;

      case 'update':
        requestContent['resource'] = model.toJSON();
        request = gapi.client.tasks[model.url].update(requestContent);
        Backbone.gapiRequest(request, method, model, options);
      break;

      case 'delete':
        requestContent['resource'] = model.toJSON();
        request = gapi.client.tasks[model.url].delete(requestContent);
        Backbone.gapiRequest(request, method, model, options);
      break;
      */
      case 'read':
        request = gapi.client.gmail.users.labels.list(options.data);
        Backbone.gapiRequest(request, method, model, options);
      break;
    }
  };

  /** リクエストの登録後の実行処理
   * @param {} request  Backbone.syncにおけるCRUD操作によって決定されたリクエストの内容
   * @param {} method
   * @param {} model
   * @param {} options
   */
  Backbone.gapiRequest = function(request, method, model, options) {
    var result;
    request.execute(function(res) {
      if (res.error) {
        if (options.error) options.error(res);
      } else if (options.success) {
        if (res.items) {
          result = res.items;
        } else {
          result = res;
        }
        // fetchなどのRESTfulな関数のsuccessと紐づけ
        options.success(result);
      }
    });
  };

  return ApiManager;
});
