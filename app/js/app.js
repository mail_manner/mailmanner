/** backbone.jsの根幹部分となるプログラム
 * main.jsの次に読み込まれるファイル
 */
define([
  'gapi'
, 'views/headerView'
, 'views/mainView'
, 'views/footerView'
, 'views/authView'
, 'views/listMenu'
, 'collections/labelCollection'
  /*
, 'routes'
, 'views/app'
, 'views/lists/menu'
, 'collections/tasklists'
, 'collections/tasks'
*/
],

function(ApiManager, headerView, mainView, footerView, authView, listMenu,labelCollection) {
  var App = function() {
    this.views.headerView = new headerView();
    this.views.mainView = new mainView();
    this.views.footerView = new footerView();
    this.views.authView = new authView(this);
    this.collections.label = new labelCollection();
    this.listMenu = new listMenu(this.views);

    this.connectGapi(); // スクリプトが読み込まれたと同時にデータの取得をする
    /*
    this.routes = new Routes();

    this.views.app = new AppView();
    this.views.app.render();
    this.views.auth = new AuthView(this);
    this.views.auth.render();
    this.views.listMenu = new ListMenuView({ collection: this.collections.lists });
    */
  };

  App.prototype = {
    views: {},
    collections: {},

    /** OAuth認証後のGoogle APIによるデータの取得
     */
    connectGapi: function() {
      var self = this;
      this.apiManager = new ApiManager(this);
      this.labelCollection = new labelCollection();

      // gapi.jsにおいて、OAuthが認証されたと同時にreadyイベントが発火される
      this.apiManager.on('ready', function() {

        // Collection.fetchの引数(options)はBackbone.syncのoptionsに渡される
        self.collections.label.fetch({ data: { userId: 'me' }, success: function(collection, res, req) {
          // successの関数に入る時点でcollectionにfetchで得られた結果が渡されている
          // self.collections.label.render(res.labels);
          self.listMenu.render(res.labels);
          // self.views.listMenu.render();
          Backbone.history.start();
        }});
      });
    }
  };

  return App;
});
