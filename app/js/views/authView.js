define([
  'text!templates/authTemplate.html'
  ], function(authTemp) {
  var authView = Backbone.View.extend({
    el: '#sign-in-container',
    template: _.template(authTemp),

    events: {
      'click #authorize-button': 'auth'
    },

    initialize: function(app) {
      this.app = app;
      this.render();
    },

    render: function() {
      this.$el.html(this.template());
      return this;
    },

    auth: function() {
      this.app.apiManager.checkAuth();
      return false;
    }
  });

  return authView;
});
