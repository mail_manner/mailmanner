/*
 * receiveMailView.js
 * 
 * 持つべき情報: title, context
 * 関数: reply
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/receiveMailTemplate.html',
], function ($, _, Backbone, mailTemplate) {
	'use strict';
	var receiveMailView = Backbone.View.extend({
		el: 'div',
		template: _.template(mailTemplate),
		events: {
			
		},
		initialize: function(){
			this.$title = 
		},
		render: function(){
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		}
	});
	return receiveMailView;
});