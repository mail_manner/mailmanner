/** header用jsファイル
 * 
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/headerTemplate.html'
], function ($, _, Backbone, headerTemp) {
	'use strict';
	var headerView = Backbone.View.extend({
		el: '#head-container',
		template: _.template(headerTemp),
		initialize: function(){
			this.render();
			//this.listenTo(this.model, 'change', this.render);
		},
		render: function(){
			this.$el.html(this.template(/*this.model.toJSON()*/));
			return this;
		}
	});
	return headerView;
});