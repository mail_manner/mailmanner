/** メイン画面のview
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/listMenuTemplate.html'
], function ($, _, Backbone, listMenuTemp) {
	// 'use strict';
	var listMenu = Backbone.View.extend({
		el: '#listMenu',
		template: _.template(listMenuTemp),
		templatelist: _.template("<ul><%= name %></ur>"),
		// events: {
		// 	'keypress #new-todo':		'createOnEnter',
		// 	'click #clear-completed':	'clearCompleted',
		// 	'click #toggle-all':		'toggleAllComplete'
		// },
		initialize: function(options){
			this.options = options || {}
			this.el = options.mainView.el;
		},
		render: function(labels){
			var self = this;
			this.$el.html(this.template());
			$.each(labels, function(index, elem) {
				if (elem.type != "system")
					self.$el.find("#labelLists").append(self.templatelist({name : elem.name}));
			});
			// this.$el.find("#labelLists").html(this.templatelist({name : labels[0].name}));
		}
	});
	return listMenu;
});