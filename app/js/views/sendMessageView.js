/*
 * .js
 * 
 * 持つべき情報: title, context
 * 関数: reply
 */
define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	'use strict';
	var sendMessageView = Backbone.View.extend({
		el: '#receiveMailList',
		template: _.template(),
		events: {
			'click #sendMessage': 'sendMessage',
			'click #closeMessage': 'closeMessage',
		},
		initialize: function(){
		
		},
		render: function(){
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},
		sendMessage: function(){
			// メッセージの送信
		},
		closeMessage: function(){
			// メッセージを閉じて、下書きに保存する

		}
	});
	return sendMessageView;
});