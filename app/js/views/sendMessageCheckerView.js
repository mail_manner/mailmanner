/*
 * sendMessageCheckerView.js
 * 
 * 持つべき情報: title, context
 * 関数: reply
 */
define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	'use strict';
	var sendMessageCheckerView = Backbone.View.extend({
		el: '#receiveMailList',
		template: _.template(),
		events: {
			
		},
		initialize: function(){
		
		},
		render: function(){
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		}
	});
	return sendMessageCheckerView;
});