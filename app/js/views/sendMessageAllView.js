/*
 * sendMessageAllView.js
 * 受信したメールの個々のモデル
 * 持つべき情報: 
 * 関数: check, close, send
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/sendMessageAllTemplate.html',
], function ($, _, Backbone, sendMessageAllTemplate) {
	'use strict';
	var sendMessageAllView = Backbone.View.extend({
		el: '#sendMessageAllView',
		template: _.template(sendMessageAllTemplate),
		events: {
			'click .checkMessage': 'checkMessage',
		},
		initialize: function(){
		
		},
		render: function(){
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},
		checkMessage: function(){

		}
	});
	return sendMessageAllView;
});