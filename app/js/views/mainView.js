/** メイン画面のview
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/mainTemplate.html'
], function ($, _, Backbone, mainTemp) {
	'use strict';
	var mainView = Backbone.View.extend({
		el: '#main-container',
		template: _.template(mainTemp),
		events: {
			'keypress #new-todo':		'createOnEnter',
			'click #clear-completed':	'clearCompleted',
			'click #toggle-all':		'toggleAllComplete'
		},
		initialize: function(){
			this.render();
		},
		render: function(){
			this.$el.html(this.template(/*this.model.toJSON()*/));
			return this;
		}
	});
	return mainView;
});