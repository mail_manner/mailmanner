/** footer用jsファイル
 * 
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/footerTemplate.html'
], function ($, _, Backbone, footerTemp) {
	'use strict';
	var footerView = Backbone.View.extend({
		el: '#foot-container',
		template: _.template(footerTemp),
		initialize: function(){
			this.render();
			//this.listenTo(this.model, 'change', this.render);
		},
		render: function(){
			this.$el.html(this.template(/*this.model.toJSON()*/));
			return this;
		}
	});
	return footerView;
});