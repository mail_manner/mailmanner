/*
 * receiveMailListView.js
 * 
 * 持つべき情報: title, context
 * 関数: reply
 */

define([
	'jquery',
	'underscore',
	'backbone',
	'gmail'
], function ($, _, Backbone, google) {
	'use strict';
	var receiveMailListView = Backbone.View.extend({
		el: '#receiveMailList',
		template: _.template(),
		events: {
			this.listenTo(receiveMailCollection, 'add', this.addOne);
			this.listenTo(receiveMailCollection, 'all', this.render);
		},
		initialize: function(){
			this.$receiveMailCollection = this.$('#receiveMailCollection');
			var $result = $("#result");
		},
		addOne: function(receiveMailModel){
			var view = new receiveMailModel({ model: receiveMailModel });
			this.$receiveMailCollection.append(view.render().el);
		},
		addAll: function(){
			this.$receiveMailCollection.empty();

			receiveMailCollection.each(this.addOne, this);
		},
		render: function(){
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		}
	});
	return receiveMailListView;
});