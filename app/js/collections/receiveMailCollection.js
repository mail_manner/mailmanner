/*
 * receiveMailCollection.js
 * 受信したメール全体のモデル
 * 持つべき情報: 
 * 関数: mailSearch
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'collections/todos',
	'views/todos',
	'text!templates/stats.html',
	'common'
], function ($, _, Backbone, Todos, TodoView, statsTemplate, Common) {
	'use strict';
	var receiveMailCollection = Backbone.Collection.extend({
		model: receiveMail,
		sort: function(){

		},
		mailSearch: function(){
			// GmailのAPI
		},
		load: function(){
			gapi.client.load("gmail","v1", function(){
				gapi.client.gmail.users.labels.list({userId : "me"}).execute(function(result , rowData) {
					console.log(result);
					console.log(rowData);
				});
			});
		}

	});

	return receiveMailCollection;
});

