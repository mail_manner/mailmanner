define(['models/labelModel'], function(labelModel) {
  var labelModel = Backbone.Collection.extend({
    model: labelModel,
    url: 'labelModel',

  render: function(labels){
    this.labels = labels;
  },

  });
  return labelModel;
});
