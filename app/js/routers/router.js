/*
 * router.js
 * 
 * 持つべき情報: 
 * 関数: 
 */
define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	'use strict';

	var router = Backbone.Router.extend({
		routes: {
			'received': 'showReceivedMailBox',
			'draft': 'showDraftBox',
			'sended' : 'showSendedMailBox',
			//'create' 
		},
		showReceivedMailBox: function(){
			console.log('showReceivedMailBox');
		},
		showDraftBox: function(){
			console.log('showDraftBox');
		},
		showSendedMailBox: function(){
			console.log('showSendedMailBox');
		}
	});

	return router;
});