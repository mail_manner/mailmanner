/** require.js読み込み用
 * htmlファイルのscriptタグから直接読み込まれる
 */

'use strict';

require.config({
	// ライブラリへの相対パス(拡張子は省略)
	paths: {
		app: 'app',
		backbone: 'libs/backbone/backbone-min',
		backboneLocalstorage: 'libs/backbone.localstorage/backbone.localStorage',
		bootstrap: 'libs/bootstrap/js/bootstrap.min',
		jasmine: 'libs/jasmine/jasmine',
		jquery: 'libs/jquery/jquery-2.1.3.min',
		text: 'libs/requirejs-text/text',
		underscore: 'libs/underscore/underscore-min'
	},
	// ライブラリの依存解決
	shim: {
		app: {
	      deps: ['backbone', 'underscore']
	    },
        backbone: {
			deps: ['underscore',　'jquery']
		},
		backboneLocalstorage: {
			deps: ['backbone']
		},
		bootstrap: {
            deps: ['jquery']
        },
    }
});

require([
	'app'
], function (App) {
	// mailmanner全体をコントロールするオブジェクトの作成
	window.mailmanner = new App();
});
