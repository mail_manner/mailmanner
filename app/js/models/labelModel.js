/** ユーザボックス内のメッセージやスレッドを分類するラベルのモデル
 * labelのデータ構造と説明: [https://developers.google.com/gmail/api/v1/reference/users/labels]
 */
define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	'use strict';

	var labelModel = Backbone.Model.extend({
		url: 'label',
		defaults: {
			id: '',
			name: '',
			messageListVisibility: '',
			labelListVisibility: '',
			type: '',
			messagesTotal: 0,
			messagesUnread: 0,
			threadsTotal: 0,
			threadsUnread: 0
		},
	});

	return labelModel;
});