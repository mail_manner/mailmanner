/** メールのメッセージモデル
　* 参考: データ構造と説明 [https://developers.google.com/gmail/api/v1/reference/users/messages]
 */
define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	'use strict';

	var messageModel = Backbone.Model.extend({
		url: 'message',
		defaults: {
			id: '',
			threadId: '',
			labelIds: {},
			snippet: '',
			historyId: '',
			payload: {},
			sizeEstimate: 0,
			raw: ''
		},
	});

	return messageModel;
});