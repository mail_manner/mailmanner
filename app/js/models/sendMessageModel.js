/*
 * sendMessageModel.js
 * 送信するメールのモデル
 * 持つべき情報: title, context
 * 関数: reply
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'collections/todos',
	'views/todos',
	'text!templates/stats.html',
	'common'
], function ($, _, Backbone, Todos, TodoView, statsTemplate, Common) {
	'use strict';
	var sendMessageModel = Backbone.Model.extend({
		// Default attributes for the todo
		// and ensure that each todo created has `title` and `completed` keys.
		defaults: {
			title: '',
			completed: false
		},
	});

	return sendMessageModel;
});