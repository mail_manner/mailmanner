/** 下書きのメッセージモデル
　* 参考: データ構造と説明 [https://developers.google.com/gmail/api/v1/reference/users/drafts]
 */

define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	'use strict';

	var draftModel = Backbone.Model.extend({
		url: 'draft',
		defaults: {
			id: '',
			messages: {}
		},
	});

	return draftModel;
});