/*
 * receiveMailModel.js
 * 受信したメールの個々のモデル
 * 持つべき情報: title, time, from, context
 * 関数:
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'collections/todos',
	'views/todos',
	'text!templates/stats.html',
	'common'
], function ($, _, Backbone, Todos, TodoView, statsTemplate, Common) {
	'use strict';

	var receiveMailModel = Backbone.Model.extend({
		// Default attributes for the todo
		// and ensure that each todo created has `title` and `completed` keys.
		defaults: {
			title: '',
			completed: false
		},
	});

	return receiveMailModel;
});