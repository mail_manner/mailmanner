/*
 * sendMessageCheckModel.js
 * 送信するメールの構文をチェックするモデル
 * 持つべき情報: title, context
 * 関数: 
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'collections/todos',
	'views/todos',
	'text!templates/stats.html',
	'common'
], function ($, _, Backbone, Todos, TodoView, statsTemplate, Common) {
	'use strict';
	var sendMessageCheckModel = Backbone.Model.extend({
		// Default attributes for the todo
		// and ensure that each todo created has `title` and `completed` keys.
		defaults: {
			title: '',
			completed: false
		},
	});

	return sendMessageCheckModel;
});