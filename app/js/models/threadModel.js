/** 会話を表現するメッセージのコレクション
 */
define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	'use strict';

	var threadModel = Backbone.Model.extend({
		url: 'thread',
		defaults: {
			id: '',
			snippet: '',
			historyId: '',
			messages: {}
		},
	});

	return threadModel;
});