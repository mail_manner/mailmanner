// 個々のtodo項目のview
var app = app || {};

// Todo項目を表すDOMの要素
app.TodoView = Backbone.View.extend({
	// タグ名
	tagName: 'li',
	// テンプレートのための関数をキャッシュする
	template: _.template($('#item-template').html()),
	// 項目における固有のDOMイベント
	events: {
		'click .toggle': 'togglecompleted',
		'dblclick label': 'edit',
		'click .destroy': 'clear',
		'keypress .edit': 'updateOnEnter',
		'blur .edit': 'close'
	},
	// モデルにおける変化を監視し再描画、破棄、不可視をする
	initialize: function(){
		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.remove);
		this.listenTo(this.model, 'visible', this.toggleVisible);		
	},
	// 項目のタイトルを再描画する
	render: function(){
		this.$el.html(this.template(this.model.toJSON()));
		this.$el.toggleClass('completed', this.model.get('completed'));
		this.$input = this.$('.edit');
		return this;
	},
	// 項目の表示/非表示を切り替える
	toggleVisible: function(){
		this.$el.toggleClass('hidden', this.isHidden());
	},
	// 項目を非表示にするべきか判定をする
	isHidden: function(){
		var isCompleted = this.model.get('completed');
		return (
			(!isCompleted && app.TodoFilter === 'completed')
			|| (isCompleted && app.TodoFilter === 'active')
		);
	},
	// モデルのcompleted属性の値をトグルする
	togglecompleted: function(){
		this.model.toggle();
	},
	// 編集モードに移行し、入力フィールドを表示する
	edit: function(){
		this.$el.addClass('editing');
		this.$input.focus();
	},
	// 編集モードを終了し、todoの項目を保存する
	close: function(){
		var value = this.$input.val().trim();

		if(value){
			this.model.save({title: value});
		}else{
			this.clear();
		}
		this.$el.removeClass('editing');
	},
	// Enterキーが押されると編集モードを終了する
	updateOnEnter: function(e){
		if(e.which === ENTER_KEY){
			this.close();
		}
	},
	// モデルをlocalStorageから消去し、ビューも破棄する
	clear: function(){
		this.model.destroy();
	}
});
