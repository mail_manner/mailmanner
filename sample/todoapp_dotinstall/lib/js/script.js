var Memo = Backbone.Model.extend({
    urlRoot: "/memo",
    idAttribute: "_id",
    defaults: {
        "content": ""
    }
});

var memo = new Memo();
 
console.log("Before save: " + JSON.stringify(memo));
console.log("isNew(): " + memo.isNew());
 
memo.save({content: "Acroquest"}, {
    success: function() {
        console.log("After save(post) memo: " + JSON.stringify(memo));
        console.log("After save(post) memo.isNew(): " + memo.isNew());
    }
});
 
console.log("After save: " + JSON.stringify(memo));
console.log("isNew(): " + memo.isNew());